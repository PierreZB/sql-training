-- https://www.hackerrank.com/challenges/symmetric-pairs/problem

SELECT S.Name
FROM STUDENTS S
LEFT JOIN FRIENDS F ON S.ID = F.ID
LEFT JOIN PACKAGES PF ON F.Friend_ID = PF.ID
LEFT JOIN PACKAGES PS ON S.ID = PS.ID
WHERE PF.Salary > PS.Salary
ORDER BY PF.Salary ASC
;

-- SELECT s.Name
-- FROM Students s, Friends f, Packages p, Packages pf
-- WHERE s.ID = f.ID AND f.Friend_ID = pf.ID
-- AND s.ID = p.ID
-- AND p.Salary < pf.Salary
-- ORDER BY pf.Salary
-- ;
