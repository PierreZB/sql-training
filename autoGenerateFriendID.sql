CREATE DEFINER=`Pierre`@`localhost` PROCEDURE `autoGenerate`()
BEGIN
  DECLARE i INT DEFAULT 0 ;
  SET autocommit = 0;
  -- Start
  WHILE
    (i < 1000000) DO -- Terminate

    REPLACE INTO tests_db_01.students(ID, Name) VALUE (i, ROUND(RAND()*10000, 0)) ;
    REPLACE INTO tests_db_01.friends (ID, Friend_ID) VALUE (i, i + 1) ;
    REPLACE INTO tests_db_01.packages (ID, Salary) VALUE (i, ROUND(RAND()*1000, 0)) ;

    SET i = i + 1 ;

    IF i%1000000=0 THEN
        COMMIT;
    END IF;
  END WHILE ;
  SET autocommit =1;
  COMMIT;
END
