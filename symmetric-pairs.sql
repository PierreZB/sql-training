-- https://www.hackerrank.com/challenges/symmetric-pairs/problem

SELECT DISTINCT a.x, a.y
FROM functions a
INNER JOIN functions b
ON a.x = b.y AND b.x = a.y
AND (a.x <> a.y OR (SELECT COUNT(*) FROM functions f WHERE f.x = a.x AND f.y = a.y) > 1)
WHERE a.x <= a.y
ORDER BY a.x ASC
;
