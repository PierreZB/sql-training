SELECT name,
    ROUND(1.0 * COUNT(DISTINCT order_id) /
    COUNT(DISTINCT delivered_to), 2) AS reorder_rate
FROM order_items
JOIN orders ON
    orders.id = order_items.order_id
GROUP BY 1
ORDER BY 2 DESC;
