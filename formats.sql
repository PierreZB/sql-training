SELECT DATE(delivery_time),
    count(*) as count_baked_goods
FROM baked_goods
GROUP BY DATE(delivery_time);


SELECT DATETIME(delivery_time, '+5 hours', '20 minutes', '2 days') as package_time
FROM baked_goods;


SELECT ROUND(distance, 2) as distance_from_market
FROM bakeries ;


SELECT id, MAX(cook_time, cool_down_time), MIN(cook_time, cool_down_time)
FROM baked_goods;


SELECT first_name || ' ' || last_name as  full_name
FROM bakeries;


SELECT REPLACE(ingredients,'enriched_','') as item_ingredients
FROM baked_goods;
