-- https://www.hackerrank.com/challenges/the-pads/problem

SELECT concat(name, '(', LEFT(occupation, 1), ')') AS field01
FROM OCCUPATIONS 
ORDER BY name ASC
;

SELECT concat('There are a total of ', COUNT(occupation), ' ', lower(occupation), 's.') AS field02
FROM OCCUPATIONS
GROUP BY occupation
ORDER BY COUNT(occupation), occupation
;