-- Daily Revenu
select
  date(created_at),
  round(sum(price), 2) as daily_rev
from purchases
where refunded_at IS NULL
group by 1
order by 1;


-- Daily Active Users
select
  date(created_at),
  count(distinct  user_id) as dau
from gameplays
group by 1
order by 1;

select
  date(created_at),
  platform,
  count(distinct user_id) as dau
from gameplays
group by 1, 2
order by 1, 2;


-- Daily Average Revenue Per Purchasing User (ARPPU)
select
  date(created_at),
  round(sum(price) / count(distinct user_id), 2) as arppu
from purchases
where refunded_at is null
group by 1
order by 1;

-- Daily Average Revenue Per User (ARPU)
With daily_revenue as (
  select
    date(created_at) as dt,
    round(sum(price), 2) as rev
  from purchases
  where refunded_at is null
  group by 1
)
select * from daily_revenue order by dt;

with daily_revenue as (
  select
    date(created_at) as dt,
    round(sum(price), 2) as rev
  from purchases
  where refunded_at is null
  group by 1
),
daily_players as (
  select
    date(created_at) as dt,
    count(distinct user_id) as players
  from gameplays
  group by 1
)
select * from daily_players order by dt;


with daily_revenue as (
  select
    date(created_at) as dt,
    round(sum(price), 2) as rev
  from purchases
  where refunded_at is null
  group by 1
),
daily_players as (
  select
    date(created_at) as dt,
    count(distinct user_id) as players
  from gameplays
  group by 1
)
select
  daily_revenue.dt,
  daily_revenue.rev / daily_players.players
from daily_revenue
  join daily_players on daily_revenue.dt = daily_players.dt;

-- 1 Day Retention
select
  date(g1.created_at) as dt,
  g1.user_id
from gameplays as g1
  join gameplays as g2 on
    g1.user_id = g2.user_id
order by 1
limit 100;


select
  date(g1.created_at) as dt,
  g1.user_id,
  g2.user_id
from gameplays as g1
  left join gameplays as g2 on
    g1.user_id = g2.user_id
    and date(g1.created_at) = date(datetime(g2.created_at, '-1 day'))
order by 1
limit 100;


SELECT
  DATE(g1.created_at) AS dt,
  ROUND(100 * COUNT(DISTINCT g2.user_id) /
  COUNT(DISTINCT g1.user_id)) AS retention
FROM gameplays AS g1
  LEFT JOIN gameplays AS g2 ON
    g1.user_id = g2.user_id
    AND DATE(g1.created_at) = DATE(DATETIME(g2.created_at, '-1 day'))
GROUP BY 1
ORDER BY 1
LIMIT 100;
